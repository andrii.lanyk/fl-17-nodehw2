const {Schema, model, SchemaTypes} = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const noteSchema = new Schema(
  {
    userId: {
      type: SchemaTypes.ObjectId,
      ref: 'user',
    },
    completed: {
      type: Boolean,
      default: false,
    },
    text: {
      type: String,
      required: [true, 'Set text for note'],
    },
    createdDate: {
      type: Date,
      default: new Date().toString(),
    },
  },
  {
    versionKey: false,
    toObject: {virtuals: true},
  },
);

noteSchema.plugin(mongoosePaginate);

const Note = model('note', noteSchema);

module.exports = Note;
