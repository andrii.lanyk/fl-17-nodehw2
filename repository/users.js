const User = require('../model/user');

const findById = async (id) => {
  return await User.findById(id);
};

const findByUsername = async (username) => {
  return await User.findOne({username});
};

const create = async (options) => {
  const user = new User(options);
  return await user.save();
};

const removeUser = async (id) => {
  const result = await User.findOneAndRemove({_id: id});
  return result;
};

const updateUser = async (body, id) => {
  const result = await User.findByIdAndUpdate({_id: id}, {password: body});
  return result;
};

const updateToken = async (id, token) => {
  return await User.updateOne({_id: id}, {token});
};

module.exports = {
  findById,
  findByUsername,
  create,
  removeUser,
  updateUser,
  updateToken,
};
