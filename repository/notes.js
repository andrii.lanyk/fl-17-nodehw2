const Note = require('../model/note');

const listOfNotes = async (userId, query) => {
  const {limit = 0, offset = 0} = query;

  const allNotes = await Note.find({userId});

  const notes = await Note.find({userId}).skip(offset).limit(limit);
  // const count = notes.length;
  const count = allNotes.length;

  return {offset: Number(offset), limit: Number(limit), count, notes};
};

const getNoteById = async (id, userId) => {
  const result = await Note.findOne({_id: id, userId}).populate({
    path: 'userId',
    select: 'id',
  });
  if (result) {
    result.userId = userId;
  }
  return result;
};

const removeNote = async (id, userId) => {
  const result = await Note.findOneAndRemove({_id: id, userId});
  return result;
};

const addNote = async (body) => {
  const result = await Note.create(body);
  return result;
};

const updateNote = async (id, body, userId) => {
  const result = await Note.findOneAndUpdate({_id: id, userId}, {...body});
  return result;
};

module.exports = {
  listOfNotes,
  getNoteById,
  removeNote,
  addNote,
  updateNote,
};
