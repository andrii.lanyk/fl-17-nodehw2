const express = require('express');
const logger = require('morgan');
const cors = require('cors');
const boolParser = require('express-query-boolean');
const helmet = require('helmet');

const notesRouter = require('./routes/notes');
const usersRouter = require('./routes/users');
const authRouter = require('./routes/auth');

const app = express();

const formatsLogger = app.get('env') === 'development' ? 'dev' : 'short';

app.use(helmet());
app.use(logger(formatsLogger));
app.use(cors());
app.use(express.json({limit: 10000}));
app.use(boolParser());

app.use('/api/notes', notesRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/auth', authRouter);

app.use((_req, res) => {
  res.status(404).json({status: 'error', code: 404, message: 'Not found'});
});

app.use((err, _req, res, _next) => {
  res.status(500).json({status: 'fail', code: 500, message: err.message});
});

module.exports = app;
