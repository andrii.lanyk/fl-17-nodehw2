const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const {ValidInfoNote} = require('../../config/constants');

const schemaNote = Joi.object({
  text: Joi.string()
    .min(ValidInfoNote.MIN_SYMBOLS)
    .max(ValidInfoNote.MAX_SYMBOLS)
    .required(),
  completed: Joi.boolean(),
});

const schemaCompletedNote = Joi.object({
  id: Joi.objectId(),
});

const schemaId = Joi.object({
  id: Joi.objectId(),
});

const validate = async (schema, obj, res, next) => {
  try {
    await schema.validateAsync(obj);
    next();
  } catch (error) {
    res.status(400).json({
      status: 'error',
      code: 400,
      message: `Field ${error.message.replace(/"/g, '')}`,
    });
  }
};

module.exports.validateNote = async (req, res, next) => {
  return await validate(schemaNote, req.body, res, next);
};

module.exports.validateStatusNote = async (req, res, next) => {
  return await validate(schemaCompletedNote, req.body, res, next);
};

module.exports.validateId = async (req, res, next) => {
  return await validate(schemaId, req.params, res, next);
};
