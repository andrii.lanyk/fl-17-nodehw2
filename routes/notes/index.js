const express = require('express');
const router = new express.Router();
const {
  getNotes,
  getNote,
  removeNote,
  saveNote,
  updateNote,
  updateStatusNote,
} = require('../../controllers/notes');
const {validateNote, validateStatusNote, validateId} = require('./validation');
const guard = require('../../helpers/guard');
const wrapError = require('../../helpers/errorHandler');

router.get('/', guard, wrapError(getNotes));

router.post('/', guard, validateNote, wrapError(saveNote));

router.get('/:id', guard, validateId, wrapError(getNote));

router.put('/:id', guard, [validateId, validateNote], wrapError(updateNote));

router.patch(
  '/:id',
  guard,
  [validateId, validateStatusNote],
  wrapError(updateStatusNote),
);

router.delete('/:id', guard, validateId, wrapError(removeNote));

module.exports = router;
