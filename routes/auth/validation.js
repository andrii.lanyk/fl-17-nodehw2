const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const {HttpCode, ValidInfoUser} = require('../../config/constants');

const schemaAuthRegistration = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().min(ValidInfoUser.MIN_PASSWORD).required(),
});

const schemaAuthLogin = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().min(ValidInfoUser.MIN_PASSWORD).required(),
});

const validate = async (schema, obj, res, next) => {
  try {
    await schema.validateAsync(obj);
    next();
  } catch (err) {
    res.status(HttpCode.BAD_REQUEST).json({
      status: 'error',
      code: HttpCode.BAD_REQUEST,
      message: `Validation error: ${err.message}`,
    });
  }
};

module.exports.validateAuthRegistration = async (req, res, next) => {
  return await validate(schemaAuthRegistration, req.body, res, next);
};

module.exports.validateAuthLogin = async (req, res, next) => {
  return await validate(schemaAuthLogin, req.body, res, next);
};
