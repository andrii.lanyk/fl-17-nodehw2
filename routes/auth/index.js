const express = require('express');
const router = new express.Router();
const {registration, login, logout} = require('../../controllers/auth');
const {validateAuthLogin, validateAuthRegistration} = require('./validation');
const guard = require('../../helpers/guard');

router.post('/register', validateAuthRegistration, registration);

router.post('/login', validateAuthLogin, login);

router.post('/logout', guard, logout);

module.exports = router;
