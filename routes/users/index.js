const express = require('express');
const router = new express.Router();
const {
  currentUser,
  deleteUser,
  changeUser,
} = require('../../controllers/users');

const guard = require('../../helpers/guard');

router.get('/', guard, currentUser);

router.delete('/', guard, deleteUser);

router.patch('/', guard, changeUser);

module.exports = router;
