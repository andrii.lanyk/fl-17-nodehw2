const Notes = require('../repository/notes');
const {HttpCode} = require('../config/constants');

const getNotes = async (req, res, _next) => {
  const userId = req.user._id;
  try {
    const result = await Notes.listOfNotes(userId, req.query);

    if (result) {
      return res.status(HttpCode.OK).json(result);
    }
    return res.status(HttpCode.BAD_REQUEST).json({
      message: 'Bad request',
    });
  } catch (error) {
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error',
    });
  }
};

const getNote = async (req, res, _next) => {
  const userId = req.user._id;
  try {
    const note = await Notes.getNoteById(req.params.id, userId);
    if (note) {
      return res.status(HttpCode.OK).json({note});
    }
    return res.status(HttpCode.BAD_REQUEST).json({
      message: 'Bad request',
    });
  } catch (error) {
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error',
    });
  }
};

const saveNote = async (req, res, _next) => {
  const userId = req.user._id;
  try {
    const note = await Notes.addNote({...req.body, userId});
    if (note) {
      return res.status(HttpCode.OK).json({message: 'Success'});
    }
    return res.status(HttpCode.BAD_REQUEST).json({
      message: 'Bad request',
    });
  } catch (error) {
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error',
    });
  }
};

const removeNote = async (req, res, _next) => {
  const userId = req.user._id;
  try {
    const note = await Notes.removeNote(req.params.id, userId);

    if (note) {
      return res.status(HttpCode.OK).json({message: 'Success'});
    }
    return res.status(HttpCode.BAD_REQUEST).json({
      message: 'Bad request',
    });
  } catch (error) {
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error',
    });
  }
};

const updateNote = async (req, res, _next) => {
  const userId = req.user._id;
  try {
    const note = await Notes.updateNote(req.params.id, req.body, userId);
    if (note) {
      return res.status(HttpCode.OK).json({message: 'Success'});
    }
    return res.status(HttpCode.BAD_REQUEST).json({
      message: 'Bad request',
    });
  } catch (error) {
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error',
    });
  }
};

const updateStatusNote = async (req, res, _next) => {
  const userId = req.user._id;
  try {
    const noteFind = await Notes.getNoteById(req.params.id, userId);
    const changeNoteCompleted = {completed: !noteFind.completed};
    const note = await Notes.updateNote(
      req.params.id,
      changeNoteCompleted,
      userId,
    );
    if (note) {
      return res.status(HttpCode.OK).json({message: 'Success'});
    }
    return res.status(HttpCode.BAD_REQUEST).json({
      message: 'Bad request',
    });
  } catch (error) {
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error',
    });
  }
};

module.exports = {
  getNotes,
  getNote,
  saveNote,
  removeNote,
  updateNote,
  updateStatusNote,
};
