const bcrypt = require('bcryptjs');
const Users = require('../repository/users');
const {HttpCode, SALT_FACTOR} = require('../config/constants');
require('dotenv').config();

const currentUser = async (req, res, _next) => {
  try {
    const {username, _id, createdDate} = req.user;
    return res.status(HttpCode.OK).json({
      user: {
        _id,
        username,
        createdDate,
      },
    });
  } catch (error) {
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error',
    });
  }
};

const deleteUser = async (req, res, _next) => {
  try {
    const userId = req.user._id;
    const user = await Users.removeUser(userId);

    if (user) {
      return res.status(200).json({message: 'Success'});
    }
    return res.status(HttpCode.BAD_REQUEST).json({
      message: 'Bad request',
    });
  } catch (error) {
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error',
    });
  }
};

const changeUser = async (req, res, _next) => {
  try {
    const {oldPassword, newPassword} = req.body;
    const {_id, username} = req.user;
    const user = await Users.findByUsername(username);
    const isValidPassword = await user.isValidPassword(oldPassword);
    if (!isValidPassword) {
      return res.status(HttpCode.BAD_REQUEST).json({
        message: 'Bad request',
      });
    }

    const salt = await bcrypt.genSalt(SALT_FACTOR);
    const updatePassword = await bcrypt.hash(newPassword, salt);
    const updateUser = await Users.updateUser(updatePassword, _id);
    if (updateUser) {
      return res.status(HttpCode.OK).json({message: 'Success'});
    }
    return res.status(HttpCode.BAD_REQUEST).json({
      message: 'Bad request',
    });
  } catch (error) {
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error',
    });
  }
};

module.exports = {
  currentUser,
  deleteUser,
  changeUser,
};
