const jwt = require('jsonwebtoken');
const Users = require('../repository/users');
const {HttpCode} = require('../config/constants');
require('dotenv').config();
const SECRET_KEY = process.env.JWT_SECRET_KEY;

const registration = async (req, res, _next) => {
  const {username, password} = req.body;
  const user = await Users.findByUsername(username);
  if (user) {
    return res.status(HttpCode.BAD_REQUEST).json({
      message: 'Username is already exist',
    });
  }
  try {
    await Users.create({username, password});
    return res.status(HttpCode.OK).json({
      message: 'Success',
    });
  } catch (error) {
    console.log('error: ', error);
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error!!!!',
    });
  }
};

const login = async (req, res, _next) => {
  try {
    const {username, password} = req.body;
    const user = await Users.findByUsername(username);
    if (!user) {
      return res.status(HttpCode.BAD_REQUEST).json({
        message: 'Invalid credentials',
      });
    }
    const isValidPassword = await user.isValidPassword(password);
    if (!isValidPassword) {
      return res.status(HttpCode.BAD_REQUEST).json({
        message: 'Invalid credentials',
      });
    }
    const id = user._id;
    const payload = {id};
    const token = jwt.sign(payload, SECRET_KEY, {expiresIn: '1h'});
    await Users.updateToken(id, token);
    return res.status(HttpCode.OK).json({
      message: 'Success',
      jwt_token: token,
    });
  } catch (error) {
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error',
    });
  }
};

const logout = async (req, res, _next) => {
  const id = req.user._id;
  try {
    await Users.updateToken(id, null);
    return res.status(HttpCode.OK).json({message: 'Success'});
  } catch (error) {
    return res.status(HttpCode.INTERNAL_SERVER_ERROR).json({
      message: 'Internal Server Error',
    });
  }
};

module.exports = {
  registration,
  login,
  logout,
};
